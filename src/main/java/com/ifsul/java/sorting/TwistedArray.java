package com.ifsul.java.sorting;

import java.util.Random;

public class TwistedArray {
    /**
     * 
     * @param items max length for twisted array
     */
    public TwistedArray(int items) {
        this.twistedArray = new Random().ints(1,101).limit(items).toArray();
    }
    
    private final int[] twistedArray;

    public int[] getTwistedArray() {
        return twistedArray;
    }
}
