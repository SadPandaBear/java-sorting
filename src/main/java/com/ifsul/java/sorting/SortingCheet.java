/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ifsul.java.sorting;

/**
 *
 * @author lucas.lentz
 */
public class SortingCheet {
    /**
     * 
     * @param vetor designed array for bubble sorting
     * @return sorted array
     */
    public static int[] bubbleSort(int vetor[]){
        boolean troca = true;
        int aux;
        while (troca) {
            troca = false;
            for (int i = 0; i < vetor.length - 1; i++) {
                if (vetor[i] > vetor[i + 1]) {
                    aux = vetor[i];
                    vetor[i] = vetor[i + 1];
                    vetor[i + 1] = aux;
                    troca = true;
                }
            }
        }
        return vetor;
    }    
    /**
     * 
     * @param vetor designed array for insertion sorting
     * @return 
     */
    public static int[] insertionSort(int[] vetor) {
        int j;
        int key;
        int i;
   
        for (j = 1; j < vetor.length; j++)
        {
               key = vetor[j];
               for (i = j - 1; (i >= 0) && (vetor[i] > key); i--)                                                                                                        
               {
                      vetor[i + 1] = vetor[i];
               }
                       vetor[i + 1] = key;
        }
        
        return vetor;
    }
    
    public static int[] quickSort(int[] vetor, int menor, int maior) {
 
		int meio = menor + (maior - menor) / 2;
		int pivot = vetor[meio];
 
		int i = menor, j = maior;
		while (i <= j) {
			while (vetor[i] < pivot) {
				i++;
			}
 
			while (vetor[j] > pivot) {
				j--;
			}
 
			if (i <= j) {
				int temp = vetor[i];
				vetor[i] = vetor[j];
				vetor[j] = temp;
				i++;
				j--;
			}
		}
 
		if (menor < j){
			quickSort(vetor, menor, j);
                }
 
		if (maior > i){
			quickSort(vetor, i, maior);
                }
                
                return vetor;
	}
    
    public static int[] shellSort (int[] vetor){
        int i, 
            j, 
            intervalo = 1, 
            value;
        
        do {
            intervalo = 3 * intervalo + 1;
        } while (intervalo < vetor.length);
        
        do {
            intervalo = intervalo / 3;
            
            for (i = intervalo; i < vetor.length; i++) {
                value = vetor [i];
                j = i - intervalo;
                
                while (j >= 0 && value < vetor [j]) {
                    vetor[j + intervalo] = vetor [j];
                    j = j - intervalo;
                }
             
                vetor [j + intervalo] = value;
            }
            
        } while (intervalo > 1);
        
        return vetor;
    }
}

