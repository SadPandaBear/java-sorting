/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ifsul.java.sorting.test;

import com.ifsul.java.sorting.SortingCheet;
import com.ifsul.java.sorting.TwistedArray;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author lucas.lentz
 */
public class SortingCheetTest {
    
    private static long startTime, endTime;
    
    private final int[] test500Array;
    private final int[] test100Array;
    private final int[] test50Array;
    private final int[] test25Array;
    
    public SortingCheetTest() {
        this.test500Array = new TwistedArray(500).getTwistedArray();
        this.test100Array = new TwistedArray(100).getTwistedArray();
        this.test50Array = new TwistedArray(50).getTwistedArray();
        this.test25Array = new TwistedArray(25).getTwistedArray();
    }
    
    private void assertSortedArray(int[] sortedArray) {
        int i = 0;
        while (i < sortedArray.length - 1) {
            assertTrue(sortedArray[i] <= sortedArray[i + 1]);
            i++;
        }
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        startTime = System.nanoTime();
    }
    
    @After
    public void tearDown() { 
        endTime = System.nanoTime();
        double total = (endTime - startTime);
        System.out.printf("Tempo de execução em nanosegundos: %s\n", total);
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void itShouldBubbleSort500LengthArray() {
        int[] sortedArray = SortingCheet.bubbleSort(test500Array);
        assertSortedArray(sortedArray);
    }
    
    @Test
    public void itShouldBubbleSort100LengthArray() {
        int[] sortedArray = SortingCheet.bubbleSort(test100Array);
        assertSortedArray(sortedArray);
    }
    
    @Test
    public void itShouldBubbleSort50LengthArray() {
        int[] sortedArray = SortingCheet.bubbleSort(test50Array);
        assertSortedArray(sortedArray);
    }
    
    @Test
    public void itShouldBubbleSort25LengthArray() {
        int[] sortedArray = SortingCheet.insertionSort(test25Array);
        assertSortedArray(sortedArray);
    }
    
    @Test
    public void itShouldInsertionSort500LengthArray() {
        int[] sortedArray = SortingCheet.insertionSort(test500Array);
        assertSortedArray(sortedArray);
    }
    
    @Test
    public void itShouldInsertionSort100LengthArray() {
        int[] sortedArray = SortingCheet.insertionSort(test100Array);
        assertSortedArray(sortedArray);
    }
    
    @Test
    public void itShouldInsertionSort50LengthArray() {
        int[] sortedArray = SortingCheet.insertionSort(test50Array);
        assertSortedArray(sortedArray);
    }
    
    @Test
    public void itShouldInsertionSort25LengthArray() {
        int[] sortedArray = SortingCheet.bubbleSort(test25Array);
        assertSortedArray(sortedArray);
    }
    
    @Test
    public void itShouldQuickSort500LengthArray() {
        int[] sortedArray = SortingCheet.quickSort(test500Array, 0, 499);
        assertSortedArray(sortedArray);
    }
    
    @Test
    public void itShouldQuickSort100LengthArray() {
        int[] sortedArray = SortingCheet.quickSort(test100Array, 0, 99);
        assertSortedArray(sortedArray);
    }
    
    @Test
    public void itShouldQuickSort50LengthArray() {
        int[] sortedArray = SortingCheet.quickSort(test50Array, 0, 49);
        assertSortedArray(sortedArray);
    }
    
    @Test
    public void itShouldQuickSort25LengthArray() {
        int[] sortedArray = SortingCheet.quickSort(test25Array, 0, 24);
        assertSortedArray(sortedArray);
    }
    
    @Test
    public void itShouldShellSort500LengthArray() {
        int[] sortedArray = SortingCheet.shellSort(test500Array);
        assertSortedArray(sortedArray);
    }
    
    @Test
    public void itShouldShellSort100LengthArray() {
        int[] sortedArray = SortingCheet.shellSort(test100Array);
        assertSortedArray(sortedArray);
    }
    
    @Test
    public void itShouldShellSort50LengthArray() {
        int[] sortedArray = SortingCheet.shellSort(test50Array);
        assertSortedArray(sortedArray);
    }
    
    @Test
    public void itShouldShellSort25LengthArray() {
        int[] sortedArray = SortingCheet.shellSort(test25Array);
        assertSortedArray(sortedArray);
    }
}
